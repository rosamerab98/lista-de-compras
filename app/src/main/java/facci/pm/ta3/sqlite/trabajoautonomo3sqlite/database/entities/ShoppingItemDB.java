package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }


    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        //con esta linea muestra el repositorio de datos en modo escritura
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        //con ContentValues se crea un nuevo mapa de valores, y
        //los nombres de cada columna son las llavas
        ContentValues valor = new ContentValues();
        //se invoca el método put con la instancia values de ContentValues
        valor.put(ShoppingElementEntry.COLUMN_NAME_TITLE, productName);
        String selection  = ShoppingElementEntry.COLUMN_NAME_TITLE+" = ?";
        //esta linea inserta una nueva fila devolviendo el valor de la llave principal
        database.insert(ShoppingElementEntry.TABLE_NAME,selection,valor);
    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
        //nuevamente se muestra el repositorio de datos en modo escritura
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        //se utiliza delete con el nombre de la tabla, y los dos siguientes parametros
        // en null para que se limpie la lista de productos
        database.delete(ShoppingElementEntry.TABLE_NAME,null,null);
    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        //nuevamente se muestra el repositorio de datos en modo escritura
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        //se crea la instancia newValues de ContentValues para insertar el
        // nuevo valor en la lista
        ContentValues newValues = new ContentValues();
        newValues.put(ShoppingElementEntry.COLUMN_NAME_TITLE,shoppingItem.getName());
        String selection = ShoppingElementEntry._ID+" = ?";
        //selectArgs representara el valor anterior al nuevo
        String [] selectionArgs = {String.valueOf(shoppingItem.getId())};
        //se utiliza update con los parametros anteriores para actualizar
        //la lista con los nuevos productos
        database.update(ShoppingElementEntry.TABLE_NAME,newValues,selection, selectionArgs);
    }

    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos
        //nuevamente se muestra el repositorio de datos en modo escritura
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        String select = ShoppingElementEntry.COLUMN_NAME_TITLE+" = ?";
        String [] selectArgs = {shoppingItem.getName()};
        //se utiliza nuevamente delete, pero esta vez colocando los dos
        //siguientes parametros para que se eliminen los Items que se especifican
        //en la MainActivity
        database.delete(ShoppingElementEntry.TABLE_NAME, select,selectArgs);
    }
}
